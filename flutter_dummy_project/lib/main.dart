import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController taskcontroller = new TextEditingController();

  List<String> _todoList = [
    "Add Tests",
    "Add Persistance",
    "Style app for iOS"
  ];

  List<bool> _checkedTest = [
    false,
    false,
    false,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo List"),
        backgroundColor: Colors.blueAccent,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addTask();
        },
        child: Icon(Icons.add),
      ),
      body: _buildTodoList(),
    );
  }

  void _addTask() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: TextFormField(
              keyboardType: TextInputType.text,
              controller: taskcontroller,
              decoration: InputDecoration(
                  hintText: "e.g. Wake Up at 8",
                  labelText: "Tasks",
                  icon: Icon(Icons.person_outline)),
            ),
            content: Text("Do you want to add Task?"),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("No")),
              FlatButton(
                  onPressed: () {
                    setState(() {
                      _todoList.add(taskcontroller.text.toString());
                      _checkedTest.add(false);
                      Navigator.pop(context);
                    });
                  },
                  child: Text("Yes")),
            ],
            elevation: 24.0,
          );
        });
  }

  Widget _buildTodoList() {
    return ListView.builder(
      itemBuilder: (context, index) {
        if (index < _todoList.length && index < _checkedTest.length) {
          return _buildTodoItem(_todoList[index], index);
        }
      },
    );
  }

  Widget _buildTodoItem(String todoList, int i) {
    return CheckboxListTile(
      title: Text(todoList),
      controlAffinity: ListTileControlAffinity.trailing,
      value: _checkedTest[i],
      onChanged: (bool value) {
        setState(() {
          _checkedTest[i] = value;
        });
      },
    );
  }

  void dispose() {
    taskcontroller.dispose();
    super.dispose();
  }
}
